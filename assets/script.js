// Assign Elements
resultHolder = document.querySelector("#result");
rollBtn = document.querySelector("#roll");

function roll() {
    let diceRolls =[];
    for(let i = 1; i <= 2; i++) {
        const roll = Math.floor(Math.random() * 6) + 1;
        diceRolls.push(roll);
        document.querySelector(`#dice-${i}`).src = `./assets/images/${roll}.png`;
    }
    resultHolder.innerHTML = `You rolled a total of ${diceRolls.reduce((a, b) => a + b, 0)}`;
}

rollBtn.addEventListener("click", roll, false);